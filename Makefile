check-var = $(if $(strip $($1)),,$(error var for "$1" is empty))

image_name := jfcarp/leverage
image_tag := 0.0.1

default: help

help: ## this help
	@awk 'BEGIN {FS = ":.*?## "} /^[\/a-zA-Z_-]+:.*?## / {sub("\\\\n",sprintf("\n%22c"," "), $$2);printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}' Makefile


build: ## Build docker container
	@docker build -t $(image_name):$(image_tag) .
	@docker build -t $(image_name):latest .


push: ## Push container to dockerhub
	@docker push $(image_name):$(image_tag)
	@docker push $(image_name):latest


run: build ## Run in docker
ifeq ($(expiration),)
	$(call check-var,symbol)
	docker run --rm -it $(image_name):$(image_tag) --symbol $(symbol) --web-site off
else
	$(call check-var,symbol)
	docker run --rm -it $(image_name):$(image_tag) --symbol $(symbol) --expiration $(expiration) --web-site off
endif


run_web: ## Run in docker with web
	docker run --rm -it -p 80:80 $(image_name):$(image_tag)


run_local: ## Run local
ifeq ($(expiration),)
	$(call check-var,symbol)
	python3 main.py --symbol $(symbol) --web-site off
else
	$(call check-var,symbol)
	python3 main.py --symbol $(symbol) --expiration $(expiration) --web-site off
endif


run_local_web: ## Run local with web
	python3 main.py

fastapi: ## Runa app using fastapi instead of flask
	uvicorn main:app --reload

%:
	@echo "ignoring rule $@" >/dev/null

.PHONY: default help build run
