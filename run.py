from app import app
from app.arguments import parse_arguments

if __name__ == "__main__":
    a = parse_arguments()
    if a.dev is True:
        from app.iex import iex
        iex()
    elif a.web_site is True:
        app.run("0.0.0.0", port=80, debug=True)
    else:
        from app.leverage import main
        main()
