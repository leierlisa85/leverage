
# How to Run
For Windows Users:
- [Install git](https://git-scm.com/download/win)
- [Install python3](https://www.python.org/downloads/windows/)
- Open Powershell
- Verify Installs
   - Run `git --version`
   - Run `python3 --version`
   - Run `python3 -m pip --version`
- Run `cd /path/to/where/you/want/code`
- Run `git clone https://gitlab.com/happymoney/leverage.git`
- Run `cd leverage`
- Run `python3 -m pip install --user --requirement requirements.txt`
- Run `python3 main.py` for faskapi or `python3 run.py` for flask
- Open browser and to to http://127.0.0.1

When you want it to end, just type Ctrl-C in the powershell console. <br />
On subsequent runs, just run `python3 run.py` unless new packages are required due to an update. <br />

To update later:
- Open powershell and cd in leverage directory
- Run `git pull`
- if necessary run `python3 -m pip install --user --requirement requirements.txt`

# Flask web site
python3 run.py --web-site

Go to http://127.0.0.1 to see input form.

Enter ticker and expiration and click submit

This should open a new tab with table showing .

One can also skip the form and go directly to the data via:

http://127.0.0.1/leverage?symbol=FB&expiration=2021-11-05

# Script args
```
usage: run.py [-s SYMBOL] [-h] [-e EXPIRATION] [-w WEB_SITE] [-d]

Calculate leverage for specific symbol and expiration.

optional arguments:
  -s SYMBOL, --symbol SYMBOL
                        Stock ticker to run against
  -h, --help            show this help message and exit
  -e EXPIRATION, --expiration EXPIRATION
                        Expiration to run against.
                        Format: YYYY-MM-DD
                        Default: closest Friday to today
  -w WEB_SITE, --web-site WEB_SITE
                        Enable website to view data in browser
  -d, --debug           Print debug information
```

# Makefile commands
```
help             this help
build            Build docker container
push             Push container to dockerhub
run              Run in docker
run_web          Run in docker with web
run_local        Run local
run_local_web    Run local with web
```

##Examples:
Run script locally for ticker FB:
```
make run local=true symbol=FB
```
Run script in docker for ticker FB and expiration 2021-10-29
```
make run symbol=FB expiration=2021-10-29
```
