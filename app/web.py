from flask import render_template, request, send_file
import pandas as pd
from io import BytesIO
from app import app, download_list
from app.arguments import parse_arguments
from app.leverage import *


@app.route('/leverage')
def index():
    form_data = request.form
    print(form_data)
    ticker = request.args.get('ticker')
    symbol = request.args.get('symbol')
    expiration = request.args.get('expiration')
    sort = request.args.get('sort')
    if ticker:
        symbol = ticker
    return render_template('table.html',
                            title='Happy Money Leverage Calculator',
                            strikes=main(symbol, expiration, sort))


@app.route('/download')
def download():
    global download_list

    if download_list == "":
        return "ERROR"
    else:
        download_ticker = download_list[0][0].split("|")[0]

    print(f'LIST: {download_list} TICK: {download_ticker}')

    #create a random Pandas dataframe
    df = pd.DataFrame(download_list, columns=[
        'Option', 'Stock Price', 'Option Price', 'Option Delta', 'Leverage'])

    df = df.set_index('Option')

    #create an output stream
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')

    #taken from the original question
    df.to_excel(writer, startrow = 0, merge_cells = False, sheet_name = download_ticker)
    workbook = writer.book
    worksheet = writer.sheets[download_ticker]
    format = workbook.add_format()
    format.set_bg_color('#eeeeee')
    worksheet.set_column(0,6,21)

    #the writer has done its job
    writer.close()

    #go back to the beginning of the stream
    output.seek(0)

    #finally return the file
    return send_file(output, download_name="{}.xlsx".format(download_ticker), as_attachment=True)


@app.route('/')
def form():
    return render_template('form.html',
                            title='Happy Money Leverage Calculator')
