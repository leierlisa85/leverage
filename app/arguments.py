from argparse import ArgumentParser, SUPPRESS, RawTextHelpFormatter
from app import app

def parse_arguments():
    # function to retrieve command line args
    parser = ArgumentParser(add_help=False, description = "Calculate leverage for specific symbol and expiration.", formatter_class=RawTextHelpFormatter)
    required = parser.add_argument_group('required arguments')
    optional = parser.add_argument_group('optional arguments')
    optional.add_argument("-t", "--ticker", help = "Stock ticker to run against")
    optional.add_argument(
        '-h',
        '--help',
        action='help',
        default=SUPPRESS,
        help='show this help message and exit'
    )
    optional.add_argument("-e", "--expiration", required=False, default=None,
                          help = '''Expiration to run against. \nFormat: YYYY-MM-DD \nDefault: closest Friday to today''')
    optional.add_argument("-w", "--web-site", required=False, default=True, help = "Enable website to view data in browser")
    optional.add_argument("-s", "--sort", required=False, action="store_true", help = "Sort by highest leverage")
    optional.add_argument("-d", "--debug", required=False, action="store_true", help = "Print debug information")
    optional.add_argument("--dev", required=False, action="store_true", help="Run in dev mode")
    return parser.parse_args()
