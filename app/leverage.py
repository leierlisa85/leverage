from requests import Session
from urllib.parse import unquote
from logging import basicConfig, DEBUG, debug
from datetime import date, timedelta
from io import BytesIO
from operator import itemgetter
from app import app, download_list
from app.arguments import parse_arguments


def request_barchart(u):
    # function to retrieve our token from barchart and run subsequent query against api
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    with Session() as s:
        s.get(
            "https://www.barchart.com/stocks/quotes/GME/volatility-greeks",
            headers=headers,
        )
        headers["X-XSRF-TOKEN"] = unquote(s.cookies["XSRF-TOKEN"])
        data = s.get(u, headers=headers).json()
        return data


def get_stock_price(symbol):
    url = f'https://www.barchart.com/proxies/core-api/v1/quotes/get?symbol={symbol}&fields=symbol,lastPrice&page=1&limit=100&raw=1'
    data = request_barchart(url)
    for d in data["data"]:
        return d["lastPrice"]


def get_options_data(symbol, expiration, stockPrice):
    optionsList = []
    page = 1

    while(True):
        try:
            debug(f'getting page: {page}')
            url = f'https://www.barchart.com/proxies/core-api/v1/options/get?symbol={symbol}&expirationDate={expiration}&fields=symbol,delta,lastPrice&page={page}&limit=100&raw=1'
            data = request_barchart(url)
            debug(data)
            if data["count"] != 0:
                for d in data["data"]:
                    debug('Symbol: {} Price: {} Delta: {}'.format(d["symbol"], stockPrice, d["lastPrice"], d["delta"]))
                    optionList = [d["symbol"], stockPrice, d["lastPrice"], d["delta"]]
                    optionsList.append(optionList)
            else:
                break
            page += 1
        except Exception as e:
            print(e)
            break
    return optionsList


def calculate_leverage(stockPrice, optionPrice, delta):
    # Delta Value of Option x Price of Underlying Security) / Price of Option
    # top half of equation delta multiplied by stock price
    t = float(delta) * float(stockPrice.replace(',',""))
    # bottom half of equation option price multiplied by number of shares
    b = float(optionPrice) * 100
    try:
        # divide to complete calculation
        l = float(t) / float(b)
        return l
    except ZeroDivisionError as e:
        # often after hourse price of zero is returned, so we account for this and return none
        return None


class Strike:
  def __init__(self, s, e, op, od, l):
    self.option = s
    self.price = e
    self.option_price = op
    self.option_delta = od
    self.leverage = l


def main(symbol = None, expiration = None, sort = False):

    global download_list

    args = parse_arguments()

    if symbol:
        args.ticker = symbol

    if expiration:
        args.expiration = expiration

    # check if debug enabled to set logging to DEBUG
    if args.debug:
        basicConfig(level=DEBUG)

    if args.sort:
        sort = "True"

    # if expiration is not given, retrieve Next Friday. If today, will use today.
    if args.expiration is None:
        today = date.today()
        friday = today + timedelta( (4-today.weekday()) % 7 )
        args.expiration = friday

    print(f'Stock: {args.ticker}')
    # Get Current stock price for symbol
    price = get_stock_price(args.ticker)
    print(f'Stock Price: {price}')
    # Get options data for symbol and expiration
    data = get_options_data(args.ticker, args.expiration, price)
    # loop over each strike in expiration and retrieve our fields

    leverageList = []

    for strike in data:
        option = strike[0]
        # use our fields to calculate leverage for strike
        leverage = calculate_leverage(strike[1], strike[2], strike[3])
        # sometimes price is 0, so we need to verify if leverage exist since we cannot divide by zero
        # seems to happen when market is closed most often
        if leverage is None:
            #leverage = "NaN"
            leverage = float(0.00)
        else:
            leverage = float(format(leverage, '.2'))
        debug("Ticker: {} Expiration: {} StockPrice: {} Option: {} OptionPrice: {} OptionDelta: {}".format(args.ticker, args.expiration, price, option, strike[2], strike[3]))

        if args.web_site is True:
            download_list.append([option,
                                 price,
                                 strike[2],
                                 strike[3],
                                 leverage])

        strike = Strike(option,
                        price,
                        strike[2],
                        strike[3],
                        leverage)

        leverageList.append(strike)

    if args.web_site is True:
        if sort == None:
            return leverageList
        elif sort == "True":
            return sorted(leverageList, key=lambda x: getattr(x, 'leverage'), reverse=True)
    else:
        if sort == "True":
            sorted_list = sorted(leverageList, key=lambda x: getattr(x, 'leverage'), reverse=True)
            for i in sorted_list:
                print("{} leverage: {:.4f}".format(i.option, i.leverage))
        else:
            for i in leverageList:
                print("{} leverage: {:.4f}".format(i.option, i.leverage))
