FROM python:3

COPY requirements.txt /opt/requirements.txt
RUN python3 -m pip install -r /opt/requirements.txt

COPY app/templates /opt/app/templates
COPY run.py /opt/run.py
COPY main.py /opt/main.py

WORKDIR /opt
ENTRYPOINT ["python3", "/opt/main.py"]
