from fastapi import FastAPI, Request, Response
from fastapi.responses import StreamingResponse
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from datetime import date, timedelta
from requests import Session
from urllib.parse import unquote
from logging import basicConfig, DEBUG, debug
import asyncio
import pandas as pd
from io import BytesIO
from starlette.responses import FileResponse
import uvicorn

app = FastAPI()
download_list = []

#app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="app/templates")

@app.get("/", response_class=HTMLResponse)
async def read_item(request: Request):
    return templates.TemplateResponse("form.html",
        {
            "request": request,
            "title": 'Happy Money Leverage Calculator'
        })

@app.get('/leverage')
async def index(ticker: str, request: Request, expiration: str = None, sort: bool = True):
    print(f'Ticker: {ticker}')
    print(f'Expiration: {expiration}')
    print(f'Sort: {sort}')
    return templates.TemplateResponse('table.html',
        {
            "request": request,
            "title": 'Happy Money Leverage Calculator',
            "strikes": main(ticker, expiration, sort)
        })

@app.get('/download')
async def download():
    global download_list

    if download_list == "":
        return "ERROR"
    else:
        download_ticker = download_list[0][0].split("|")[0]
        download_expiration = download_list[0][0].split("|")[1]

    print(f'LIST: {download_list} TICK: {download_ticker} EXP: {download_expiration}')

    #create a random Pandas dataframe
    df = pd.DataFrame(download_list, columns=[
        'Option', 'Stock Price', 'Option Price', 'Option Delta', 'Leverage'])

    df = df.set_index('Option')

    #create an output stream
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine='xlsxwriter')

    #taken from the original question
    df.to_excel(writer, startrow = 0, merge_cells = False, sheet_name = download_ticker)
    workbook = writer.book
    worksheet = writer.sheets[download_ticker]
    format = workbook.add_format()
    format.set_bg_color('#eeeeee')
    worksheet.set_column(0,6,21)

    #the writer has done its job
    writer.close()

    #go back to the beginning of the stream
    output.seek(0)

    #finally return the file
    #return send_file(output, download_name="{}.xlsx".format(download_ticker), as_attachment=True)
    #return FileResponse(output, media_type='application/octet-stream',filename="{}.xlsx".format(download_ticker))
    response = StreamingResponse(output, media_type="application/vnd.ms-excel")
    response.headers["Content-Disposition"] = "attachment; filename={}-{}.xlsx".format(download_ticker, download_expiration)
    return response

@app.get("/healthcheck/")
def healthcheck():
    return 'Health - OK'

def request_barchart(u):
    # function to retrieve our token from barchart and run subsequent query against api
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:86.0) Gecko/20100101 Firefox/86.0"
    }
    with Session() as s:
        s.get(
            "https://www.barchart.com/stocks/quotes/GME/volatility-greeks",
            headers=headers,
        )
        headers["X-XSRF-TOKEN"] = unquote(s.cookies["XSRF-TOKEN"])
        data = s.get(u, headers=headers).json()
        return data


def get_stock_price(symbol):
    url = f'https://www.barchart.com/proxies/core-api/v1/quotes/get?symbol={symbol}&fields=symbol,lastPrice&page=1&limit=100&raw=1'
    data = request_barchart(url)
    for d in data["data"]:
        return d["lastPrice"]


def get_options_data(symbol, expiration, stockPrice):
    optionsList = []
    page = 1

    while(True):
        try:
            debug(f'getting page: {page}')
            url = f'https://www.barchart.com/proxies/core-api/v1/options/get?symbol={symbol}&expirationDate={expiration}&fields=symbol,delta,lastPrice&page={page}&limit=100&raw=1'
            data = request_barchart(url)
            debug(data)
            if data["count"] != 0:
                for d in data["data"]:
                    debug('Symbol: {} Price: {} Delta: {}'.format(d["symbol"], stockPrice, d["lastPrice"], d["delta"]))
                    optionList = [d["symbol"], stockPrice, d["lastPrice"], d["delta"]]
                    optionsList.append(optionList)
            else:
                break
            page += 1
        except Exception as e:
            print(e)
            break
    return optionsList


def calculate_leverage(stockPrice, optionPrice, delta):
    # Delta Value of Option x Price of Underlying Security) / Price of Option
    # top half of equation delta multiplied by stock price
    t = float(delta) * float(stockPrice.replace(',',""))
    # bottom half of equation option price multiplied by number of shares
    b = float(optionPrice) * 100
    try:
        # divide to complete calculation
        l = float(t) / float(b)
        return l
    except ZeroDivisionError as e:
        # often after hourse price of zero is returned, so we account for this and return none
        return None


class Strike:
  def __init__(self, s, e, op, od, l):
    self.option = s
    self.price = e
    self.option_price = op
    self.option_delta = od
    self.leverage = l


def main(ticker = None, expiration = None, sort = False):

    global download_list

    # if expiration is not given, retrieve Next Friday. If today, will use today.
    if expiration is None:
        today = date.today()
        friday = today + timedelta( (4-today.weekday()) % 7 )
        expiration = friday

    print(f'Stock: {ticker}')
    # Get Current stock price for symbol
    price = get_stock_price(ticker)
    print(f'Stock Price: {price}')
    # Get options data for symbol and expiration
    data = get_options_data(ticker, expiration, price)
    # loop over each strike in expiration and retrieve our fields

    leverageList = []

    for strike in data:
        option = strike[0]
        # use our fields to calculate leverage for strike
        leverage = calculate_leverage(strike[1], strike[2], strike[3])
        # sometimes price is 0, so we need to verify if leverage exist since we cannot divide by zero
        # seems to happen when market is closed most often
        if leverage is None:
            #leverage = "NaN"
            leverage = float(0.00)
        else:
            leverage = float(format(leverage, '.2'))
        debug("Ticker: {} Expiration: {} StockPrice: {} Option: {} OptionPrice: {} OptionDelta: {}".format(ticker, expiration, price, option, strike[2], strike[3]))

        download_list.append([option,
                              price,
                              strike[2],
                              strike[3],
                              leverage])

        strike = Strike(option,
                        price,
                        strike[2],
                        strike[3],
                        leverage)

        leverageList.append(strike)

    if sort == False:
        for i in leverageList:
            print("{} leverage: {:.4f}".format(i.option, i.leverage))
        return leverageList
    elif sort == True:
        sorted_list = sorted(leverageList, key=lambda x: getattr(x, 'leverage'), reverse=True)
        for i in sorted_list:
            print("{} leverage: {:.4f}".format(i.option, i.leverage))
        return sorted_list

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=80)
